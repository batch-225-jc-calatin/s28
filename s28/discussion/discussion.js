// MongoDB Operations
// For creating or inserting data into database

db.users.insert({
    firstName: "Ely",
    lastName: "Buendia",
    age: 50,
    contact: {
        phone: "534776",
        email: "ely@eraserheads.com"
    },
    courses: ["CSS", "Javascript", "Python"],
    department: "none"
})
// ==========================================
// For insert Many

db.users.insertMany([
    {
        firstName: "Chito",
        lastName: "Miranda",
        age: 43,
        contact: {
            phone: "5347786",
            email: "chito@parokya.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Francis",
        lastName: "Magalona",
        age: 61,
        contact: {
            phone: "5347786",
            email: "francisM@email.com"
        },
        courses: ["React", "Laravel", "SASS"],
        department: "none"
    }
])




// ==========================================

// for querying all the data in database
// For finding documents

// Find All

db.users.find();


// Find [one]
db.users.find({fistName: "Francis", age: 61});

//============================================


// Delete [one]

db.users.deleteOne({
    firstName: "Chito"
});

// Delete [Many]

db.users.deleteMany({
    department: "DOH"
});

// =========================================
// update [one]
db.users.updateOne(
    {
        firstName: "Francis"
    },
    {
        $set: {
            lastName: "Esguerra"
        }
    }
);


// update [many]

db.users.updateMany(
    {
        department: "none"
    },
    {
        $set: {
            department: "HR"
        }
    }
);